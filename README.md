# Rebase interactif

## Introduction

A l'aide de la commande `git log --oneline`, observez les commits écrits pour l'ajout d'une nouvelle fonctionnalité.
```
4a31c1a (HEAD -> main) fixing some route tests
99a3102 fix typo on route
36e85c1 Work In Progress
3524056 adding new tests
2515af2 create new route for /
c3b8e04 add modle function
1701e97 create user controller
e9c25c6 createFiles
103b4af Create App packages
```

Vous allez devoir utiliser la commande `git rebase -i <commit>` pour réécrire l'histoire de cette fonctionnalité.

```
pick e9c25c6 createFiles
pick 1701e97 create user controller
pick c3b8e04 add modle function
pick 2515af2 create new route for /
pick 3524056 adding new tests
pick 36e85c1 Work In Progress
pick 99a3102 fix typo on route
pick 4a31c1a fixing some route tests

# Rebase 103b4af..4a31c1a onto 103b4af (8 commands)
#
# Commands:
# p, pick <commit> = use commit
# r, reword <commit> = use commit, but edit the commit message
# e, edit <commit> = use commit, but stop for amending
# s, squash <commit> = use commit, but meld into previous commit
# f, fixup [-C | -c] <commit> = like "squash" but keep only the previous
#                    commit's log message, unless -C is used, in which case
#                    keep only this commit's message; -c is same as -C but
#                    opens the editor
```
## 1. Reword

Utilisez le `reword` pour corriger la typo dans le nom du commit `add modle function` pour la changer en `add model function`.

## 2. Edit 

Editez les commits suivants :
 * `createFiles`
 * `adding new tests`
 * `Work In Progress`

Pour chacun d'entre eux, créer plusieurs commits :
 * 1 commit pour tout ce qui concerne la `Route`, 
 * 1 commit pour tout ce qui concerne le `Controller`,
 * 1 commit pour tout ce qui concerne le `Model`.

*Remarque : à cette étape, vous devriez avoir un grand nombre de commits.*

## 3. Move

Utilisez le rebase interactif pour rassembler, les uns à la suite des autres, les commits qui concernent chaque partie (`Route`, `Controller`, `Model`)

## 4. Fixup

Utilisez le `fixup` pour fusionner les commits qui concernent chaque partie (`Route`, `Controller`, `Model`).

## 5. Résultat

Avec la commande `git log --oneline` vous devriez avoir : 

```
create user route & associated tests
create user model & associated tests
create user controller & associated tests
Create App packages
```
